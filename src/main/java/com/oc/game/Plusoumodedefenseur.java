package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Plusoumodedefenseur {
    Scanner sc = new Scanner(System.in);
    Config c = Config.getInstance();
    int longCombi = c.getLongCombi();
    boolean verifdeveloppeur = c.isModeDeveloppeur();
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Plusoumoinsdefenseur avec le mode 2 c'est l'ordi qui joue pour nous.
     *@author thomas
     * @version 12.0.1
     * @param min prend la valeur la plus petite
     * @param max prend la valeur la plus grande
     * @return formule decothomie getRandomInRange
     * @exception IllegalArgumentException si le min est suprieur au max
     */
    int getRandomNumberInRange(int min, int max) {
        if (min > max) {
            Logger.error("le min est plus grand que le max.");
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * methode defenseur l'ordi va tenter de trouver la combinaision du jeu. il y a rien a faire durant le jeu.
     *@param tourjeu2  int tourjeu2 sert a definir le nombre de tour que se deroule la partie
     *@param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     *@param resultatdefense  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *@param tableaurandom  int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *@param tableauproposition int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *@param args pour lancer afficher la solution si le modedev est active.
     */
    public void defenseur(int tourjeu2, int[] tabsolutionforce, int[] tableaurandom, String[] resultatdefense, int[] tableauproposition, String[] args) {
        if ((args.length > 0) && (args[0].equals("modeDev"))) {
            c.setModeDeveloppeur(true);
        }
        if (c.isModeDeveloppeur() == true) {
            Logger.info("le mode developpeur est activé");
            System.out.println("----------------------------------------------------------");
            System.out.println("le mode developpeur est activé, voici la combinaision secrete :" + Arrays.toString(tabsolutionforce));
            System.out.println("----------------------------------------------------------");
        }
        Logger.info("Les propositions faite par l'ordi.");
        Random r = new Random();
        for (int i = 0; i < 4; i++) {
            if (tourjeu2 == 0) {
                Logger.info("Première fois que l'ordi remplit une proposition");
                tableaurandom[i] = r.nextInt(9) + 1;
            }
            tableauproposition[i] = tableaurandom[i];
        }
        for (int i = 0; i < 4; i++) {
            if ((tableauproposition[i]) == tabsolutionforce[i]) {
                resultatdefense[i] = "=";
                tableaurandom[i] = tableauproposition[i];
            }
            if ((tableauproposition[i]) > tabsolutionforce[i]) {
                resultatdefense[i] = "-";
                tableaurandom[i] = getRandomNumberInRange(1, tableauproposition[i] - 1);
            }
            if ((tableauproposition[i]) < tabsolutionforce[i]) {
                resultatdefense[i] = "+";
                tableaurandom[i] = getRandomNumberInRange(tableauproposition[i] + 1, 9);
            }
        }
        Logger.info("La proposition de l'ordi et le resultatdefense" + Arrays.toString(tableauproposition) + " " + Arrays.toString(resultatdefense));
        System.out.println("Vous avez tentez : " + Arrays.toString(tableauproposition));
        System.out.println("Résultat : " + Arrays.toString(resultatdefense));
        System.out.println("---------------------");
    }
}