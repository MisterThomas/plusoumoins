package com.oc.game;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
    /**
     * class Config sert a iniatiliser nos variables deja appeller dans d'autre classe
     *
     * @return modeDeveloppeur si le modedev est active.
     * @param modeDeveloppeur
     * @return adapte la longueur des combinaison en fonction de la valeur de la longueur combinaision
     * @param longCombi om prend en parametre la variable longCombi est on adapte la longueur en fonction de celle que l'on a defini dans le fichier propertises
     * @author thomas
     * @version 12.0.1
     */
    private static Config c = new Config();
    int longCombi;
    boolean modeDeveloppeur;

    private Config() {
        loadProperties();
    }

    public static Config getInstance() {
        return c;
    }

    public void loadProperties() {
        Properties prop = new Properties();
        try {
            InputStream in = getClass().getClassLoader().getResourceAsStream("config.properties");
            prop.load(in);
            longCombi = Integer.parseInt(prop.getProperty("longueurCombinasion"));
            modeDeveloppeur = Boolean.parseBoolean(prop.getProperty("modeDeveloppeur"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isModeDeveloppeur() {

        return modeDeveloppeur;
    }

    public void setModeDeveloppeur(boolean modeDeveloppeur) {
        this.modeDeveloppeur = modeDeveloppeur;
    }

    public int getLongCombi() {
        return longCombi;
    }

    public void setLongCombi(int longCombi) {
        this.longCombi = longCombi;
    }
}
