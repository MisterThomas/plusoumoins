package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Scanner;

public class Plusoumoinsattaque {
    Scanner sc = new Scanner(System.in);
    Config c = Config.getInstance();
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();
    int longCombi = c.getLongCombi();

    public static boolean isNumeric(String strNum) {
        try {
            int value = Integer.parseInt(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * class PLusoumoinsattaque jeu en mode attaque le joueur fais des propositions(reponses).
     *void attaque methode pour le mode 1 attaque  ou le joueur fait des propositions(reponses) avec en args tabsolutionforce,resultat,tourjeu1
     * @author thomas
     * @version 12.0.1
     * @param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     * @param resultat  String[] resultat est le resultat d'attaque.
     * @param tourjeu1  int tourjeu1  sert a definir le nombre de tour que se deroule la partie
     * @param args  affiche la solution si le mode dev est active
     */
    public void attaque(int[] tabsolutionforce, String[] resultat, int tourjeu1, String[] args) {
        if ((args.length > 0) && (args[0].equals("modeDev"))) {
            c.setModeDeveloppeur(true);
        }
        if (c.isModeDeveloppeur() == true) {
            Logger.info("le mode developpeur est activé");
            System.out.println("----------------------------------------------------------");
            System.out.println("le mode developpeur est activé, voici la combinaision secrete :" + Arrays.toString(tabsolutionforce));
            System.out.println("----------------------------------------------------------");
        }
        Logger.info("Le joueur fait une proposition et il y a des différentes solutions possibles pour réparer cette erreur.");
        boolean isCombinaisionvalide = true;
        String reponse = null;
        do {
            isCombinaisionvalide = true;
            Logger.info("Le joueur saisi une combinaison.");
            System.out.print("Saisir votre combinaison.");
            reponse = sc.nextLine();
            if (reponse.length() < longCombi) {
                Logger.info("Vous n'avez pas assez de caractère pour votre réponse.");
                System.out.println("Vous n'avez pas assez de caractère pour votre réponse.");
                isCombinaisionvalide = false;
            }
            if (reponse.length() > longCombi) {
                Logger.info("Vous avez trop caractère pour votre réponse.");
                System.out.println("Vous avez trop de caractère pour votre réponse.");
                isCombinaisionvalide = false;
            }
            if (!isNumeric(reponse)) {
                Logger.info("Le joueur a saisi incorrect.");
                System.out.println("Combinaison incorrect");
                isCombinaisionvalide = false;
            }
        } while (!isCombinaisionvalide);
        for (int tour = 0; tour < 4; tour++) {
            if (Character.getNumericValue(reponse.charAt(tour)) == tabsolutionforce[tour]) {
                resultat[tour] = "=";
            }
            if (Character.getNumericValue(reponse.charAt(tour)) > tabsolutionforce[tour]) {
                resultat[tour] = "-";
            }
            if (Character.getNumericValue(reponse.charAt(tour)) < tabsolutionforce[tour]) {
                resultat[tour] = "+";
            }
        }
        Logger.info("La variable du résultat joueur est affichée " + Arrays.toString(resultat));
        System.out.println("Résultat : " + Arrays.toString(resultat));
        System.out.println("---------------------");
    }
}

