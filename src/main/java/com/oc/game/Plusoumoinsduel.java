package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Plusoumoinsduel {
    Config c = Config.getInstance();
    int longCombi = c.getLongCombi();
    boolean verifdeveloppeur = c.isModeDeveloppeur();
    Scanner sc = new Scanner(System.in);

    public static boolean isNumeric(String strNum) {
        try {
            int value = Integer.parseInt(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Plusoumoinsduel avec le mode 3 c'est l'ordi et le joueur  qui joue tour a tour.
     *@author thomas
     @version 12.0.1
     *@param min prend la valeur la plus petite
     *@param max prend la valeur la plus grande
     *@return formule decothomie getRandomInRange
     * @exception IllegalArgumentException si le min est suprieur au max
     */

    int getRandomNumberInRange(int min, int max) {
        if (min > max) {
            Logger.error("le min est plus grand que le max.");
            throw new IllegalArgumentException("max must be greater than min");
        }


        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * methode attaque ou le joueur doit tenter un tour sur deux de trouver la combinaision secrete.
     *@param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     *@param resultat  String[] resultat est le resultat d'attaque.
     *@param toujeu3   int tourjeu3  sert a definir le nombre de tour que se deroule la partie
     *@param args  affiche la solution si le mode dev est active
     */
    public void attaque(int[] tabsolutionforce, String[] resultat, int toujeu3, String[] args) {
        if ((args.length > 0) && (args[0].equals("modeDev"))) {
            c.setModeDeveloppeur(true);
        }
        if (c.isModeDeveloppeur() == true) {
            Logger.info("le mode developpeur est activé");
            System.out.println("----------------------------------------------------------");
            System.out.println("le mode developpeur est activé, voici la combinaision secrete :" + Arrays.toString(tabsolutionforce));
            System.out.println("----------------------------------------------------------");
        }
        if (toujeu3 == 0) {
            System.out.println("Bienvenue dans le jeu en mode 3 duel sous cette phrase. Vous pouvez entrer votre première proposition");

        }
        Logger.info("Le joueur fait une proposition et il y a des différentes solutions possibles pour réparer cette erreur.");
        boolean isCombinaisionvalide = true;
        String reponse = null;
        do {
            isCombinaisionvalide = true;
            Logger.info("Le joueur saisi une combinaison.");
            System.out.print("Saisir votre combinaison.");
            reponse = sc.nextLine();
            if (reponse.length() < longCombi) {
                Logger.info("Vous n'avez pas assez de caractère pour votre réponse.");
                System.out.println("Vous n'avez pas assez de caractère pour votre réponse.");
                isCombinaisionvalide = false;
            }
            if (reponse.length() > longCombi) {
                Logger.info("Vous avez trop caractère pour votre réponse.");
                System.out.println("Vous avez trop de caractère pour votre réponse.");
                isCombinaisionvalide = false;
            }
            if (!isNumeric(reponse)) {
                Logger.info("Le joueur a saisi incorrect.");
                System.out.println("Combinaison incorrect");
                isCombinaisionvalide = false;
            }
        } while (!isCombinaisionvalide);
        Logger.info("Le joueur fait une proposition");
        System.out.print("Vous avez tentez : ");
        while (reponse.length() < longCombi || reponse.length() > longCombi) {
            if (reponse.length() < longCombi) {
                System.out.println("Vous n'avez pas assez de caractére pour votre reponse");
            }
            if (reponse.length() > longCombi) {
                System.out.println("Vous avez trop de caractére pour votre reponse");
            }
        }
        for (int tour = 0; tour < 4; tour++) {
            if (Character.getNumericValue(reponse.charAt(tour)) == tabsolutionforce[tour]) {
                resultat[tour] = "=";
            }
            if (Character.getNumericValue(reponse.charAt(tour)) > tabsolutionforce[tour]) {
                resultat[tour] = "-";
            }
            if (Character.getNumericValue(reponse.charAt(tour)) < tabsolutionforce[tour]) {
                resultat[tour] = "+";
            }
        }
        Logger.info("La variable du résultat joueur est affichée: " + Arrays.toString(resultat));
        System.out.println("Résultat : " + Arrays.toString(resultat));
        System.out.println("---------------------");
    }

    /**
     * methode defenseur l'ordi doit trouver la solution. il sera aider de la decothomie
     *@param tourjeu3  int tourjeu2 sert a definir le nombre de tour que se deroule la partie
     *@param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     *@param resultatdefense  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *@param tableaurandom  int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     * @param tableauproposition int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     */
    public void defenseur(int tourjeu3, int[] tabsolutionforce, int[] tableaurandom, String[] resultatdefense, int[] tableauproposition) {
        Random r = new Random();
        for (int i = 0; i < 4; ++i) {
            if (tourjeu3 == 1) {
                tableaurandom[i] = r.nextInt(9) + 1;
            }
            tableauproposition[i] = tableaurandom[i];
        }

        for (int i = 0; i < 4; ++i) {
            if (tableauproposition[i] == tabsolutionforce[i]) {
                resultatdefense[i] = "=";
                tableaurandom[i] = tableauproposition[i];
            }

            if (tableauproposition[i] > tabsolutionforce[i]) {
                resultatdefense[i] = "-";
                tableaurandom[i] = getRandomNumberInRange(1, tableauproposition[i] - 1);
            }

            if (tableauproposition[i] < tabsolutionforce[i]) {
                resultatdefense[i] = "+";
                tableaurandom[i] = getRandomNumberInRange(tableauproposition[i] + 1, 9);
            }
        }
        Logger.info("La proposition de l'ordi et le resultatdefense: " + Arrays.toString(tableauproposition) + " " + Arrays.toString(resultatdefense));
        System.out.println("Vous avez tentez : " + Arrays.toString(tableauproposition));
        System.out.println("Résultat : " + Arrays.toString(resultatdefense));
        System.out.println("---------------------");
    }
}
//   }

//   }
