package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Scanner;

public class Choixjeu {
    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Choixjeugame permet de faire le choix entre les methode et le mode de jeu 1,2,3 avec en args tous les variables des trois jeux.
     *
     * void jeudemarrage sert a lancer les differents modes de jeu avec la methode jeucommence
     * @param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     * @param resultat  String[] resultat est le resultat d'attaque.
     * @param resultatdefense  String[] resultatdefense est le resultat de l'ordi en mode defense.
     * @param tableaurandom  int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     * @param tableauproposition  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     * @param choix car choix permet au joueur de faire son choix de jeu qui veut lancer.
     * @param args pour lancer afficher la solution si le modedev est active.
     * @author thomas
     * @version 12.0.1
     */
    void Choixjeugame(int[] tabsolutionforce, String[] resultat, String[] resultatdefense, int[] tableaurandom, int[] tableauproposition, char choix, String[] args) {

        Plusoumoinsattaque Plusoumoinsattaque = new Plusoumoinsattaque();
        Plusoumodedefenseur Plusoumodedefenseur = new Plusoumodedefenseur();
        Plusoumoinsduel Plusoumoinsduel = new Plusoumoinsduel();
        Jeudemarrage Jeudemarrage = new Jeudemarrage();
        Config c = Config.getInstance();
        int longCombi = c.getLongCombi();
        if (Character.getNumericValue(choix) == 1) {
            Logger.info("L'utilisateur a fais le choix du premier jeu.");
            for (int tourjeu1 = 0; tourjeu1 < 10; tourjeu1++) {
                Plusoumoinsattaque.attaque(tabsolutionforce, resultat, tourjeu1, args);
                boolean correct = true;
                for (int checkresultat1 = 0; checkresultat1 < resultat.length; checkresultat1++) {
                    if (resultat[checkresultat1] != "=") {
                        correct = false;
                        break;
                    }
                }
                if ((correct)) {
                    Logger.info("Le joueur a gagné.");
                    System.out.println("Bravo vous avez gagné.");
                    Logger.info("Le joueur peut rejouer");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("Si oui mettez 'o' ou non 'n'.");
                    char ouiounonrejouer1 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer1 == 'o') {
                        Logger.info("le joueur rejoue.");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer1 == 'n') {
                        Logger.info("Le joueur quitte le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else if (tourjeu1 == 9 && !correct) {
                    Logger.info("Le joueur a perdu et la combinaison s'affiche.");
                    System.out.println("Vous avez perdu la combinastion était :");
                    System.out.println(Arrays.toString(tabsolutionforce));
                    System.out.println("--------------------------------------");
                    Logger.info("Le joueur peut rejouer.");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("Si oui mettez 'o' ou non 'n'.");
                    char ouiounonrejouer1 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer1 == 'o') {
                        Logger.info("Le joueur rejoue.");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer1 == 'n') {
                        Logger.info("Le joueur quitte le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else {
                    Logger.info("Le joueur n'a pas gagné et fait une nouvelle proposition.");
                    System.out.println("Vous êtes à tant de prospositions : " + (tourjeu1 + 1 + 1) + " votre resultat est : ");
                }
            }

        } else if (Character.getNumericValue(choix) == 2) {
            Logger.info("Le joueur fait le choix du deuxième jeu.");
            for (int tourjeu2 = 0; tourjeu2 < 10; tourjeu2++) {
                Plusoumodedefenseur.defenseur(tourjeu2, tabsolutionforce, tableaurandom, resultatdefense, tableauproposition, args);
                boolean correct = true;
                for (int checkresultat2 = 0; checkresultat2 < resultatdefense.length; checkresultat2++) {
                    if (resultatdefense[checkresultat2] != "=") {
                        correct = false;
                        break;
                    }
                }
                if ((correct)) {
                    Logger.info("L'ordi a gagné.");
                    System.out.println("Bravo l'ordi a gagné.");
                    Logger.info("Le joueur peut rejouer.");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("Si oui mettez 'o' ou non 'n'.");
                    char ouiounonrejouer2 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer2 == 'o') {
                        Logger.info("Le joueur rejoue .");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer2 == 'n') {
                        Logger.info("Le joueur quitte le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else if (tourjeu2 == 9 && !correct) {
                    Logger.info("L'ordi a perdu et la combinaison s'affiche.");
                    System.out.println("Vous avez perdu la combinaison était :");
                    System.out.println(Arrays.toString(tabsolutionforce));
                    System.out.println("--------------------------------------");
                    Logger.info("Le joueur peut rejouer.");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("Si oui mettez 'o' ou non 'n'.");
                    char ouiounonrejouer2 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer2 == 'o') {
                        Logger.info("Le joueur rejoue.");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer2 == 'n') {
                        Logger.info("Le joueur quitte le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else {
                    Logger.info("Le joueur n'a pas trouvé la combinaison durant sa proposition.");
                    System.out.println("Vous êtes à tant de prospositions : " + (tourjeu2 + 1 + 1) + " votre resultat est: ");
                }
            }
        } else if (Character.getNumericValue(choix) == 3) {
            Logger.info("Le joueur fait le choix du troisième jeu.");
            for (int tourjeu3 = 0; tourjeu3 < 10; tourjeu3++) {
                if (tourjeu3 % 2 == 0) {
                    Logger.info("Le joueur joue en pair au mode attaque.");
                    Plusoumoinsduel.attaque(tabsolutionforce, resultat, tourjeu3, args);
                } else {
                    Logger.info("L'ordi joue en mode impair.");
                    Plusoumoinsduel.defenseur(tourjeu3, tabsolutionforce, tableaurandom, resultatdefense, tableauproposition);
                }

                boolean correctauto = true;
                for (int checkresultat3attaque = 0; checkresultat3attaque < resultat.length; checkresultat3attaque++) {
                    if (resultat[checkresultat3attaque] != "=") {
                        correctauto = false;
                        break;
                    }
                }
                boolean correctdefense = true;
                for (int checkresultat3defense = 0; checkresultat3defense < resultatdefense.length; checkresultat3defense++) {
                    if (resultatdefense[checkresultat3defense] != "=") {
                        correctdefense = false;
                        break;
                    }
                }
                if ((correctauto) || (correctdefense)) {
                    if ((correctauto)) {
                        Logger.info("Le joueur a gagné.");
                        System.out.println("Bravo vous avez gagné.");
                        System.out.println("Voulez-vous rejouer à un jeu ?");
                        System.out.println("Si oui mettez 'o' ou non 'n'.");
                        char ouiounonrejouer = sc.nextLine().charAt(0);
                        if (ouiounonrejouer == 'o') {
                            Logger.info("Le joueur rejoue.");
                            Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                        } else if (ouiounonrejouer == 'n') {
                            Logger.info("Le joueur va quitter le jeu.");
                            System.out.println("Au revoir et à bientôt.");
                            break;
                        }
                    }
                    if ((correctdefense) || (correctdefense)) {
                        Logger.info("L'ordi a gagné.");
                        System.out.println("L'ordi a gagné.");
                        Logger.info("Le joueur peut rejouer.");
                        System.out.println("Voulez-vous rejouer à un jeu ?");
                        System.out.println("Si oui mettez 'o' ou non 'n'.");
                    }
                    char ouiounonrejouer3 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer3 == 'o') {
                        Logger.info("Le joueur rejoue.");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer3 == 'n') {
                        Logger.info("Le joueur quitte le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else if ((tourjeu3 == 9 && !correctauto) || (tourjeu3 == 9 && !correctdefense)) {
                    Logger.info("Le joueur a perdu et la combinaison s'affiche. ");
                    System.out.println("Vous avez tous les deux perdu la combinaison était: ");
                    System.out.println(Arrays.toString(tabsolutionforce));
                    System.out.println("--------------------------------------");
                    Logger.info("Le joueur peut rejouer.");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("Si oui mettez 'o' ou non 'n'.");
                    char ouiounonrejouer3 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer3 == 'o') {
                        Logger.info("Le joueur rejoue.");
                        Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, args);
                    } else if (ouiounonrejouer3 == 'n') {
                        Logger.info("Le joueur va quitter le jeu.");
                        System.out.println("Au revoir et à bientôt.");
                        break;
                    }
                } else {
                    Logger.info("Le joueur n'a pas trouvé la bonne combinaison il faut une nouvelle proposition.");
                    System.out.println("Vous êtes à tant de prospositions : " + (tourjeu3 + 1 + 1) + " votre resultat est: ");
                }
            }
        }
    }
}
