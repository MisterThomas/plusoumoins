package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.util.Random;
import java.util.Scanner;

public class Jeudemarrage {
    //FileOutputStream filestream = null;
    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Jeudemarrage sert a rejouer à un des jeux apres une victoire ou defaite.
     * Choixjeu.Choixjeugame sert a un lancer un jeu parmi les trois methodes et classe des trois jeux 1 attaque, 2 defense, 3 duel avec toute les variable en args en dessous
     *
     * @param tabsolutionforce   int[] tabsolutionforce est la combinastion secrete.
     * @param resultat           String[] resultat est le resultat d'attaque.
     * @param resultatdefense    String[] resultatdefense est le resultat de l'ordi en mode defense.
     * @param tableaurandom      int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     * @param tableauproposition int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     * @param args               pour lancer afficher la solution si le modedev est active.
     * @author thomas
     * @version 12.0.1
     */
    void jeucommence(int[] tabsolutionforce, String[] resultat, String[] resultatdefense, int[] tableaurandom, int[] tableauproposition, String[] args) {
        Logger.info("Le joueur fait son choix de jeu");
        Choixjeu Choixjeu = new Choixjeu();
        Config c = Config.getInstance();
        int longCombinaison = c.getLongCombi();
        tabsolutionforce = new int[longCombinaison];
        Random r = new Random();
        tableaurandom = new int[longCombinaison];
        Logger.info("Première fois que l'ordi remplit une proposition");
        Logger.info("Les propositions faite par random");
        for (int i = 0; i < longCombinaison; i++) {
            tableaurandom[i] = r.nextInt(9) + 1;
            tabsolutionforce[i] = tableaurandom[i];
        }
        System.out.println("----------------------------------------------------------");
        System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu : ");
        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode attaque tape 1.");
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode defenseur tape 2.");
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode duel tape 3.");
        char choix = sc.nextLine().charAt(0);
        Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix, args);
        while (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
            Logger.info("Le joueur n'a pas rentré un chiffre entre 1 et 3.");
            System.out.println("Ce n'est pas un nombre.");
            System.out.println("Voulez-vous refaire une proposition ? si oui alors 'o' ou non alors 'n' :");
            Logger.info("On propose au joueur de refaire une proposition.");
            char ouiounon = sc.nextLine().charAt(0);
            while (ouiounon == 'o') {
                Logger.info("Le joueur souhaite refaire une proposition entre 1 et 3 pour jouer.");
                System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu :");
                System.out.println();
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode attaque tape 1.");
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode defenseur tape 2.");
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode duel tape 3.");
                System.out.println("----------------------------------------------------");
                choix = sc.nextLine().charAt(0);
                if (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
                    Logger.info("Le joueur n'a pas rentré un chiffre entre 1 et 3.");
                    System.out.println("Ce n'est pas un nombre.");
                    System.out.println("Voulez-vous refaire une proposition ? si oui alors 'o' ou non alors 'n' :");
                    Logger.info("Le joueur a fait le choix de quitter le jeu.");
                    ouiounon = sc.nextLine().charAt(0);
                }
                if (ouiounon == 'n') {
                    Logger.info("Le joueur a fait le choix que le jeu est fini.");
                    System.out.println("Au revoir et à bientôt.");
                    break;
                }
                Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix, args);
            }
            if (ouiounon == 'n') {
                Logger.info("Le joueur a fait le choix que le jeu est fini.");
                System.out.println("Au revoir et à bientôt.");
                break;
            }
        }
    }
}
