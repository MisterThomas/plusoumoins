package com.oc.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.Scanner;

public class Main {
    /**
     * class Main avec deroulement du jeu au début .
     *
     * @author thomas
     * @version 12.0.1
     *
     * avec class choixJeugame appelle on peut apres choisir son jeu.
     * @param tabsolutionforce  int[] tabsolutionforce est la combinastion secrete.
     * @param resultat  String[] resultat est le resultat d'attaque.
     * @param resultatdefense  String[] resultatdefense est le resultat de l'ordi en mode defense.
     * @param tableaurandom  int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     * @param tableauproposition  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     * @param choix car choix permet au joueur de faire son choix de jeu qui veut lancer.
     * @param args pour lancer afficher la solution si le modedev est active.
     */
    private static final Logger Logger = LogManager.getLogger();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Logger.info("Démarrage du programme.");
        Config c = Config.getInstance();
        //FileOutputStream filestream = null;
        int longueurCombination = c.getLongCombi();
        int[] tabsolutionforce = new int[longueurCombination];
        Random r = new Random();
        int[] tableaurandom = new int[longueurCombination];
        Logger.info("Première fois que l'ordi remplit une proposition");
        Logger.info("Les propositions faite par random");
        for (int i = 0; i < longueurCombination; i++) {
            tableaurandom[i] = r.nextInt(9) + 1;
            tabsolutionforce[i] = tableaurandom[i];
        }
        String[] resultat = new String[longueurCombination];
        String[] resultatdefense = new String[longueurCombination];
        new Plusoumoinsattaque();
        new Plusoumodedefenseur();
        new Plusoumoinsduel();
        Choixjeu Choixjeu = new Choixjeu();
        int[] tableauproposition = new int[longueurCombination];
        System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu :");
        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode attaque tape 1.");
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode défenseur tape 2.");
        System.out.println("----------------------------------------------------");
        System.out.println("Si tu veux le mode duel tape 3.");
        System.out.println();
        Logger.info("Le joueur choisi le mode de jeu.");
        char choix = sc.nextLine().charAt(0);
        Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix, args);
        Logger.info("le joueur commence a joue et choisi le mode developpeur ou non");
        while (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
            System.out.println("Ce n'est pas un nombre.");
            System.out.println("Voulez-vous refaire une proposition ? si oui alors 'o' ou non alors 'n' : ");
            Logger.info("Le joueur peut refaire une proposition s'il veut jouer ou non. ");
            char ouiounon = sc.nextLine().charAt(0);
            while (ouiounon == 'o') {
                Logger.info("Le joueur souhaite refaire une proposition entre 1 et 3 pour jouer.");
                System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu :");
                System.out.println();
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode attaque tape 1.");
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode défenseur tape 2.");
                System.out.println("----------------------------------------------------");
                System.out.println("Si tu veux le mode duel tape 3.");
                System.out.println("----------------------------------------------------");
                choix = sc.nextLine().charAt(0);
                if (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
                    Logger.info("Il a encore fait une proposition avec un nombre en dehors de 1 et 3.");
                    System.out.println("Ce n'est pas un nombre. ");
                    System.out.println("Voulez-vous refaire une proposition ? si oui alors 'o' ou non alors 'n' :");
                    ouiounon = sc.nextLine().charAt(0);
                }
                if (ouiounon == 'n') {
                    Logger.info("Le joueur a fait le choix de quitter le jeu.");
                    System.out.println("Au revoir et à bientôt");
                    break;
                }
                Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix, args);
            }
            if (ouiounon == 'n') {
                Logger.info("Le joueur a fait le choix de quitter le jeu.");
                System.out.println("Au revoir et à bientôt");
                break;
            }
        }

    }
}
